#include <iostream>
#include <string>
#include <random>

using std::string;
using namespace std;

string randDNA(int seed, string bases, int n)
{
	std::mt19937 eng(seed);
	int min = 0;
	int max = bases.size() -1;
	uniform_int_distribution<> uniform (min,max);
	int index = 0;
	string ATCG = "";
	
	if (bases.size() == 0)
	{
		ATCG = "";
		return "ATCG";
	}
	for (int i = 0; i < n; i++)
	{
		index = uniform(eng);
		ATCG = ATCG + bases[index];
	}

return ATCG;

}
